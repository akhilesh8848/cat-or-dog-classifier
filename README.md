This is a simple Classifier that uses Convolutional Neural Network to classify images as either cat or dog. It uses python with Keras. Trained with Keras using TensorFlow backend.
